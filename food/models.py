from django.db import models

# Create your models here.
class Item(models.Model):

    def __str__(self):
        return self.Item_name

    Item_name = models.CharField(max_length=200)
    Item_desc = models.CharField(max_length=200)
    Item_price = models.IntegerField()
    Item_avalablity = models.BooleanField(default=True)
    Item_image = models.CharField(max_length=500, default="https://www.melbournefoodandwine.com.au/image-tools.php?w=560&h=440&src=/recipes/recipe-placeholder.jpg")
    