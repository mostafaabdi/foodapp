# Generated by Django 3.0.7 on 2020-09-09 11:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('food', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='Item_avalablity',
            field=models.BooleanField(default=True),
        ),
    ]
