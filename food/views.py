from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Item
from .forms import ItemForm
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.utils.translation import gettext as _
from django.core.paginator import Paginator
# Create your views here.
from django.db.models import Q

# def index(request):
#     context = {
#         'item_list': Item.objects.all()
#     }
#     return render(request, 'food/index.html', context)
order = '-date_posted'

class IndexClassView(ListView):
    model = Item
    template_name= 'food/index.html'
    context_object_name = 'item_list'
    
    paginate_by = 2
    
    def get_queryset(self):
        search_query = self.request.GET.get('foodsearch')
        if search_query:
            return Item.objects.filter(Item_name__icontains=search_query)
        return Item.objects.all()

def item(request):
    out = _('<h1>This is a view</h1>')
    return HttpResponse(out)


class DetailClassView(DetailView):
    model = Item
    template_name = 'food/detail.html'

# def detail(request, item_id):
#     context = {
#         'item': Item.objects.get(pk=item_id)
#     }
#     return render(request, 'food/detail.html', context)


def create_item(request):
    form = ItemForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('food:index')

    return render(request, 'food/create-item.html', {'form': form})


def update_item(request, id):
    item = Item.objects.get(id=id)
    form = ItemForm(request.POST or None, instance=item)
    if form.is_valid():
        form.save()
        return redirect('food:index')

    return render(request, 'food/create-item.html', {'form': form, 'item': item})


def delete_item(request, id):
    item = Item.objects.get(id=id)
    if request.method == 'POST':
        item.delete()
        return redirect('food:index')

    return render(request, 'food/delete-item.html', {'item': item})


