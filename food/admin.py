from django.contrib import admin
from food.models import Item
from django.utils.translation import gettext as _
# Register your models here.
class foodAdmin(admin.ModelAdmin):
    
    def change_desc(self,request,queryset):
        queryset.update(Item_desc='None')


    list_display = (
        'Item_name', 'Item_desc', 'Item_price'
    )
    search_fields = ('Item_name', 'Item_desc')
    actions = ('change_desc',)
    list_editable = ('Item_price',)
    fields = ('Item_name', 'Item_desc',)

admin.site.register(Item, foodAdmin)

admin.site.site_header = _('Food App Admin Panel')
admin.site.site_title = _("FoodApp")

admin.site.index_title = _("Food App Index")

