from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import userCreation

# Create your views here.

def createUser(request):
    
    if request.method == 'POST':
        form = userCreation(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request,f"Your account {username} has succesfully created!")
            return redirect('food:index')
    else:
        form = userCreation()
    return render(request, 'users/createuser.html', { 'form':form } )


@login_required 
def profilePage(request):
    return render(request, 'users/profile.html')